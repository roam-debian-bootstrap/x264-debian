#!/bin/sh
# Script modified from upstream source for Debian packaging since packaging
# won't include .git repository.
echo '#define X264_VERSION " r2412 d7e6896"'
echo '#define X264_POINTVER "0.142.2412 d7e6896"'
